import numpy as np
import cv2
import argparse


def codifica(imagem, mensagem, plano):
    """Codifica a mensagem em um plano da imagem."""
    cod = bin(int.from_bytes(mensagem.encode(), 'big'))
    cod = np.asarray(list('0' + cod[2:]), np.uint8)

    L = cod.shape[0] # Deve ser um múltiplo de 8.

    out = imagem.copy().reshape(-1)

    # Zera a parte do plano de bits a ser utilizada.
    # Note que de L a L + 8, restará o limitador \0.
    out[:L + 8] = out[:L + 8] & (255 - 2**plano)

    # Soma o código no início da imagem.
    out[:L] += cod << plano

    return out.reshape(imagem.shape)


def main():

    parser = argparse.ArgumentParser(description='Codifica mensagem de texto em imagem.')

    parser.add_argument('IMAGEM', action='store', nargs=1, type=str,
        help='arquivo da imagem que será utilizada'
    )
    
    parser.add_argument('MENSAGEM', action='store', nargs=1, type=str,
        help='nome do arquivo de texto contendo a mensagem'
    )

    parser.add_argument('PLANO', action='store', nargs=1, type=int,
        help='plano de bits a ser modificado'
    )

    parser.add_argument('SAIDA', action='store', nargs=1, type=str,
        help='arquivo para salvar imagem de saída'
    )

    args = parser.parse_args()


    # Extração da entrada:
    entrada = cv2.imread(args.IMAGEM[0])

    with open(args.MENSAGEM[0], 'r') as arq:
        msg = ''.join(arq.readlines())

    plano = args.PLANO[0]


    # Codificação:
    saida = codifica(entrada, msg, plano)

    cv2.imwrite(args.SAIDA[0], saida)


if __name__ == '__main__':
    main()
