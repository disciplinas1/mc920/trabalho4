import numpy as np
import cv2
import argparse
import sys


def plano_bits(array, plano):
    """Retorna o plano do array."""
    out = array.copy()

    out = out & (2**plano)
    out = out >> plano

    return out


def decodifica(imagem, plano):
    """Decodifica a imagem do plano."""
    msg = ''.join(str(x) for x in plano_bits(imagem, plano).reshape(-1))

    # Search in the right positions for \0:
    for byte in range(0, len(msg), 8):
        if msg[byte:byte + 8] == '00000000':
            msg = '0b' + msg[1:byte]
            break

    msg_int = int(msg, 2)

    return msg_int.to_bytes((msg_int.bit_length() + 7) // 8, 'big').decode()


def main():

    parser = argparse.ArgumentParser(description='Decodifica mensagem de texto de imagem.')

    parser.add_argument('IMAGEM', action='store', nargs=1, type=str,
        help='arquivo da imagem que será utilizada'
    )
    
    parser.add_argument('PLANO', action='store', nargs=1, type=int,
        help='plano de bits a ser verificado'
    )

    parser.add_argument('SAIDA', action='store', nargs=1, type=str,
        help='arquivo para salvar mensagem'
    )

    args = parser.parse_args()


    # Extração da entrada:
    entrada = cv2.imread(args.IMAGEM[0])

    plano = args.PLANO[0]

    saida = args.SAIDA[0]


    # Decodificação:
    msg = decodifica(entrada, plano)

    # print(msg)

    with open(saida, 'w') as arq:
        std = sys.stdout
        sys.stdout = arq

        print(msg)

        sys.stdout = std


if __name__ == '__main__':
    main()
